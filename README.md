# 模型驱动低代码平台的功能演示Demo：多表联动的库存管理系统

#### 介绍
使用活字格低代码开发平台，以模型驱动的方式开发多表联通的库存管理系统。
（Demo中仅实现数据初始化、库存查询、月结、出库功能）

#### 了解更多
* [从产品官网免费下载](https://www.grapecity.com.cn/solutions/huozige)
* [将项目clone到本地](https://help.grapecity.com.cn/pages/viewpage.action?pageId=56531929)
* [了解更多低代码知识](https://help.grapecity.com.cn/display/lowcode)
